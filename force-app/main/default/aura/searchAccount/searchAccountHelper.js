({
    ListAccount: function(component, event) {
       
        var action = component.get("c.listAccount");

        var pageSize = component.get("v.pageSize");

        var pageNumber = component.get("v.pageNumber");
    
        console.log('pageSize ---->' + pageSize)
        console.log('pageNumber ---->' + pageNumber)
    
        action.setParams({
                 'pageNumber':  pageNumber,
                 'recordToDisply': pageSize 
         });
        
        action.setCallback(this, function(response) {
             var result = response.getReturnValue();
             var state = response.getState();
         console.log('result ---->' + JSON.stringify(result))
             if (state === "SUCCESS") {
               
            
                var pagesArray = [];
            
            // Math.ceil(parseInt(result.total) / parseInt(lmt))

            var pageNumberMath = Math.ceil(parseInt(result.total) / parseInt(result.pageSize));

			for(var i=1; i<= pageNumberMath; i++){
				var obj = {};
				obj['label'] = i.toString();
				obj['value'] = i.toString();
				pagesArray.push(obj);
            }		

               
                component.set("v.page", pagesArray);
                component.set("v.pageSize", result.pageSize);
                component.set("v.total", result.total);
                component.set("v.item", result.accounts);
                
             }
        });
        $A.enqueueAction(action);
    },

   
    pagepagetionHelper : function(component, event) {

        var action = component.get("c.listAccount");

        var pageSize = component.get("v.pageSize");


        var pageNumber  = event.getSource().get("v.label");

    
        console.log('pageSize ---->' + pageSize)
        console.log('pageNumber ---->' + pageNumber)
        
    
        action.setParams({
            'pageNumber':  pageNumber,
            'recordToDisply': pageSize 
    });
   
   action.setCallback(this, function(response) {
        var result = response.getReturnValue();
        var state = response.getState();
    console.log('result ---->' + JSON.stringify(result))
        if (state === "SUCCESS") {
          
       
           var pagesArray = [];
       
       // Math.ceil(parseInt(result.total) / parseInt(lmt))

       var pageNumberMath = Math.ceil(parseInt(result.total) / parseInt(result.pageSize));

       for(var i=1; i<= pageNumberMath; i++){
           var obj = {};
           obj['label'] = i.toString();
           obj['value'] = i.toString();
           pagesArray.push(obj);
       }		

          
           component.set("v.page", pagesArray);
           component.set("v.pageSize", result.pageSize);
           component.set("v.total", result.total);
           component.set("v.item", result.accounts);
           
        }
   });
   $A.enqueueAction(action);
    },
    
    SearchAccountHelper: function(component, event) {
        var action = component.get("c.searchAccounts");
        console.log('searchAccounts')
        action.setParams({
            'searchKeyword': component.get("v.searchKeyword")
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
              // hide spinner when response coming from server 
              component.find("Id_spinner").set("v.class" , 'slds-hide');
              var state = response.getState();
              if (state === "SUCCESS") {
                  var storeResponse = response.getReturnValue();
                  component.set("v.item", storeResponse); 
              }else if (state === "ERROR") {
                  var errors = response.getError();
                  if (errors) {
                      if (errors[0] && errors[0].message) {
                          alert("Error message: " + errors[0].message);
                      }
                  } else {
                      alert("Unknown error");
                  }
              }
          });
          $A.enqueueAction(action);
    },
  
})