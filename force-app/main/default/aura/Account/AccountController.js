({
    doInit: function(component, event, helper) {
       helper.ListAccount(component, event);
    },

    Search: function(component, event, helper) {
        var searchKeyWord = component.get("v.searchKeyWord");
          if(searchKeyWord != null && searchKeyWord != ' ' && searchKeyWord != 'undefined'){
            helper.ListAccount(component, event);
        }
    },
    changePage: function(component, event, helper) {
        helper.pagepagetionHelper(component, event);
    },


})