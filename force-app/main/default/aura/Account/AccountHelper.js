({
    ListAccount: function(component, event) {
       
      
        var action = component.get("c.listAccount");
        var pageSize = component.get("v.pageSize");
        var pageNumber = component.get("v.pageNumber");
        var searchKeyWord = component.get("v.searchKeyWord");
    	
        //setParams
        action.setParams({
                 	'pageNumber':  pageNumber,
                 	'pageSize': pageSize ,
            		'searchKeyWord': searchKeyWord 
         });
       
         console.log('#searchKeyWord ' + searchKeyWord );
        //setCallback
        action.setCallback(this, function(response) {
             var result = response.getReturnValue();
             var state = response.getState();
   
             if (state === "SUCCESS") {
                var pagesArray = [];
            	var pageNumberMath = Math.ceil(parseInt(result.total) / parseInt(result.pageSize));
               
				for(var i=1; i<= pageNumberMath; i++){
				var obj = {};
				obj['label'] = i.toString();
				pagesArray.push(obj);
            	}	
                //set attribute value
                component.set("v.page", pagesArray);
                component.set("v.pageSize", result.pageSize);
                component.set("v.total", result.total);
                component.set("v.item", result.accounts); 
                component.set("v.searchKeyWord", searchKeyWord); 
             }
        });
        $A.enqueueAction(action);
    },

   
    pagepagetionHelper : function(component, event) {
        var action = component.get("c.listAccount");
        var pageSize = component.get("v.pageSize");
        var pageNumber  = event.getSource().get("v.label");
        var searchKeyWord = component.get("v.searchKeyWord");
        action.setParams({
            'pageNumber':  pageNumber,
            'recordToDisply': pageSize ,
            'searchKeyWord': searchKeyWord 
    	});
   
   action.setCallback(this, function(response) {
        var result = response.getReturnValue();
        var state = response.getState();
       
        if (state === "SUCCESS") {
        var pagesArray = [];
      	var pageNumberMath = Math.ceil(parseInt(result.total) / parseInt(result.pageSize));

       for(var i=1; i<= pageNumberMath; i++){
           var obj = {};
           obj['label'] = i.toString();
           pagesArray.push(obj);
       }		
           component.set("v.page", pagesArray);
           component.set("v.pageSize", result.pageSize);
           component.set("v.total", result.total);
           component.set("v.item", result.accounts);
           component.set("v.searchKeyWord", searchKeyWord); 
           
        }
   });
   $A.enqueueAction(action);
    },
   
})