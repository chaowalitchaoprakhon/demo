public with sharing class searchAccountController {    
    @AuraEnabled
    public static Response listAccount(Integer pageNumber ,Integer recordToDisply) {
        Integer pageSize = recordToDisply;
        Integer offset = ((Integer)pageNumber-1) * pageSize;
        Response obj = new Response ();
        obj.pageSize = pageSize;
        obj.page = offset; 
        obj.total = [SELECT count() FROM account];
        obj.accounts = [SELECT Id, Name FROM account ORDER BY Name LIMIT :recordToDisply OFFSET :offset]; 
     return obj;
    }
    @AuraEnabled
    public static List < account > fetchAccount(String searchKeyWord) {
    String searchKey = searchKeyWord + '%';
    List < Account > returnList = new List < Account > ();
    List < Account > lstOfAccount = [select id, Name, Type, Industry, Phone, Fax from account
                                   where Name LIKE: searchKey LIMIT 500];
 
            for (Account acc: lstOfAccount) {
                returnList.add(acc);
            }
        return returnList;
    }

    public class Response {
         @AuraEnabled public Integer pageSize {get; set;}
         @AuraEnabled public Integer page {get;set;}
        @AuraEnabled public Integer total {get;set;}
        @AuraEnabled public List<Account> accounts = new List<Account>();
    }
   }