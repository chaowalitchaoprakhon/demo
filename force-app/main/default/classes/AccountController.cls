public with sharing class AccountController{    
    
    @AuraEnabled
    public static Response listAccount(Integer pageNumber ,Integer pageSize,String searchKeyWord) {
        Integer offset = ((Integer)pageNumber-1) * pageSize;
        Response obj = new Response ();
        obj.page = offset; 
        if(searchKeyWord != null && searchKeyWord != ' ' && searchKeyWord != 'undefined'){
               String searchKey = searchKeyWord + '%';
               obj.pageSize = pageSize;
               obj.total = [SELECT count() FROM account  where Name LIKE: searchKey LIMIT: pageSize];
               obj.accounts = [select Name ,Type from account where Name LIKE: searchKey LIMIT: pageSize]; 
             
        }else{
                obj.pageSize = pageSize;
                obj.total = [SELECT count() FROM account];
        		obj.accounts = [SELECT  Name ,Type  FROM account ORDER BY Name LIMIT :pageSize OFFSET :offset]; 
        }   
     return obj;
    }
    
    public class Response {
         @AuraEnabled public Integer pageSize {get; set;}
         @AuraEnabled public Integer page {get;set;}
         @AuraEnabled public Integer total {get;set;}
         @AuraEnabled public List <Account> accounts = new List<Account>();
    }
    
   }